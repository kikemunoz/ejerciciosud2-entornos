package proyectoUD2;

import java.util.Scanner;

public class ProyectoUD2 {

	public static void main(String[] args) {
		Scanner escaner = new Scanner(System.in);

		int opcion;
		do {
			System.out.println("__________________________________________________________");
			System.out.println("Ejercicio 5 de la primera parte de la pr�ctica de entornos");
			System.out.println("__________________________________________________________");
			System.out.println("        MEN�                 ");
			System.out.println("1.- M�todo equals()");
			System.out.println("2.- M�todo charAt()");
			System.out.println("3.- M�todo length()");
			System.out.println("4.- Salir");
			System.out.println("Elige una de las opciones");
			System.out.println("__________________________________________________________");

			opcion = escaner.nextInt();
			switch (opcion) {
			case 1:
				escaner.nextLine();
				System.out.println("Dame una cadena");
				String cadena1 = escaner.nextLine();

				System.out.println("Dame otra cadena");
				String cadena2 = escaner.nextLine();

				if (cadena1.equals(cadena2)) {
					System.out.println("Las dos frases son iguales");
				} else {
					System.out.println("Las frases no coinciden");
				}

				break;
			case 2:
				escaner.nextLine();
				System.out.println("Introduce una palabra");
				String palabra = escaner.nextLine();
				System.out.println("La palabra introducida comienza por " + palabra.charAt(0));
				break;
			case 3:
				escaner.nextLine();
				System.out.println("Escribe una cadena");
				String cadena = escaner.nextLine();

				int longitudCadena = cadena.length();
				System.out.println("La longitud de la cadena introducida es de " +longitudCadena + " caracteres.");
				break;
			case 4:
				System.out.println("ADI�S");
				System.exit(0);
				break;
			default:
				System.out.println("Opci�n incorrecta");
			}
		} while (opcion != 4);

		escaner.close();

	}

}
